USERID=$(shell id -u)

DOCKER_COMPOSE_CMD=docker-compose
DOCKER_CMD=docker
DOCKER_APP=front

default: docker-install

# Docker
docker-login:
	$(DOCKER_CMD) login registry.gitlab.com

docker-build:
	$(DOCKER_COMPOSE_CMD) build

docker-start:
	$(DOCKER_COMPOSE_CMD) up -d

docker-rm: docker-stop
	$(DOCKER_COMPOSE_CMD) rm -f

docker-stop:
	$(DOCKER_COMPOSE_CMD) stop

docker-restart: docker-rm docker-start docker-fix-permissions

docker-install: docker-build docker-start docker-composer docker-install-assets docker-fix-permissions

docker-re-install: docker-rm docker-install

docker-composer:
	@$(MAKE) -s exec command="composer install --no-interaction"

docker-install-assets:
	@$(MAKE) -s docker-exec command="yarn install"
	@$(MAKE) -s docker-exec command="php bin/console ckeditor:install --no-interaction"
	@$(MAKE) -s docker-exec command="php bin/console assets:install --symlink web --no-interaction"
	@$(MAKE) -s docker-exec command="php bin/console assetic:dump --no-interaction"
	@$(MAKE) -s docker-exec command="yarn gulp"

docker-exec:
    ifndef command
		@echo "parameter \"command\" is not defined\n	make exec command=cache:clear"
    else
		$(DOCKER_COMPOSE_CMD) exec $(DOCKER_APP) $(command)
    endif

docker-fix-permissions:
	$(DOCKER_COMPOSE_CMD) exec $(DOCKER_APP) setfacl -dR -m u:apache:rwX -m u:$(whoami):rwX var
	$(DOCKER_COMPOSE_CMD) exec $(DOCKER_APP) setfacl -R -m u:apache:rwX -m u:$(whoami):rwX var

docker-ips:
	$(DOCKER_COMPOSE_CMD) ps -q | xargs -n 1 $(DOCKER_CMD) inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} {{ .Name }}' | sed 's/ \// /'

docker-connect:
	@$(MAKE) -s docker-exec command=bash

# Symfony
cache-clear:
	php bin/console doctrine:cache:clear-metadata
	php bin/console doctrine:cache:clear-query
	php bin/console doctrine:cache:clear-result
	php bin/console cache:clear

change-password:
    ifndef email
		@echo "parameter \"email\" is not defined\n	make change-password email=pnab@email.com"
    else
		php bin/console fos:user:change-password $(email)
    endif

# Tests
test-db-create:
	php bin/console doctrine:database:drop --force --env=test
	php bin/console doctrine:database:create --env=test
	php bin/console doctrine:schema:update --force --env=test

# Log
log:
	tail -f var/logs/dev.log

apache-log:
	tail -f /var/log/httpd/error.log

analyze-code:
	./analyze-code.sh

# Tools
fix-project-permissions:
	sudo chmod -R 755 Migrations app src vendor node_modules

ngrok:
	ngrok http -host-header=rewrite pnab.localhost:80

add-hosts:
    ifeq ($(USERID),0)
		@echo "127.0.1.1    pnab.localhost" >> /etc/hosts
		@echo "127.0.1.1    pnab.phpmyadmin.localhost" >> /etc/hosts
		@echo "127.0.1.1    pnab.maildev.localhost" >> /etc/hosts
		@$(MAKE) -s show-hosts
    else
		@echo "You are not root, run this target as root please"
    endif

show-hosts:
	@echo "http://pnab.localhost/"
	@echo "http://pnab.phpmyadmin.localhost/"
	@echo "http://pnab.maildev.localhost/"
